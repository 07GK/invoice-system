<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->foreignId('client_id');
            $table->string('name');
            $table->string('desc');
            $table->tinyInteger('is_hourly_based');
            $table->integer('rate_per_hour')->nullable();
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
        });        

        Schema::create('clients', function (Blueprint $table) {
            $table->id();        
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone_no');
            $table->string('address');
            $table->string('gstin');
            $table->string('place_of_supply');
            $table->string('currency');
            $table->string('state_code');
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
        });        

        Schema::create('taxes', function (Blueprint $table) {
            $table->id();        
            $table->string('name');
            $table->string('percentage');
            $table->foreignId('client_id');
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
        });

        Schema::create('company_details', function (Blueprint $table) {
            $table->id();        
            $table->string('field');        
            $table->string('value');        
            $table->timestamps();
        });

        Schema::create('invoices', function (Blueprint $table) {
            $table->id();        
            $table->foreignId('client_id');        
            $table->string('sr_no');        
            $table->date('invoice_date');        
            $table->date('due_date');        
            $table->tinyInteger('is_active')->default(1);       
            $table->timestamps();
        });

        Schema::create('invoice_projects', function (Blueprint $table) {
            $table->id();        
            $table->string('invoice_id');                         
            $table->string('project_id');        
            $table->string('project_desc');        
            $table->tinyInteger('is_hourly_based');       
            $table->string('rate_per_hour')->nullable();       
            $table->string('total_hours')->nullable(); 
            $table->string('total_amount');       
            $table->timestamps();
        });

        Schema::create('invoice_taxes', function (Blueprint $table) {
            $table->id();        
            $table->string('invoice_id');                    
            $table->string('tax_name');        
            $table->string('tax_per');                    
            $table->string('tax_amount');       
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tables');
    }
}
