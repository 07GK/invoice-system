<?php
  
return [
    //Menus
    'clients' => 'Clients',
    'projects' => 'projects',
    'profile' => 'profile',
    'invoice' => 'invoice',
    
    //Buttons
    'new_records' => 'New Record',
    'add' => 'Add',
    'reset' => 'Reset',
    'update' => 'Update',
    'edit' => 'Edit',
    'view' => 'View',

    //Navigation
    
    //clients
    'manage_clients'=>'Manage Clients',    
    'add_clients'=>'Add Client',
    'edit_clients'=>'Edit Client',
    'view_clients'=>'View Clients',
    'deleted_clients'=>'Deleted Clients',
    
    //projects
    'manage_projects'=>'Manage Projects',
    'add_projects'=>'Add Project',
    'edit_projects'=>'Edit Project',
    'view_projects'=>'View Projects',
    'deleted_projects'=>'Deleted Projects',

    //invoice
    'manage_invoice'=>'Manage Invoices',
    'gen_invoice'=>'Generate Invoice',
    'edit_invoice'=>'Edit Invoice',
    'view_invoice'=>'View Invoice',
    'deleted_invoice'=>'Deleted Invoice',
];

?>