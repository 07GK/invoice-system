<!--begin::Fonts-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
<!--end::Fonts-->
<!--begin::Page Custom Styles(used by this page)-->
<link  href="{{ asset('assets/css/login.css?v=7.0.3') }}" rel="stylesheet" type="text/css" />
<link  href="{{ asset('assets/css/datatables.bundle.css?v=7.0.3') }}" rel="stylesheet" type="text/css" />
<!--end::Page Custom Styles-->
<!--begin::Global Theme Styles(used by all pages)-->
<link href="{{ asset('assets/css/plugins.bundle.css?v=7.0.3') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/prismjs.bundle.css?v=7.0.3') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/style.bundle.css?v=7.0.3') }}" rel="stylesheet" type="text/css" />
<!--end::Global Theme Styles-->
<!--begin::Layout Themes(used by all pages)-->
<!--end::Layout Themes-->
<link rel="shortcut icon" href="{{ asset('assets/media/logos/logo.png') }}" type="image/x-icon" />	
<!-- <link rel="shortcut icon" href="favicon.ico" > -->
