<html>
    <head>
        <title>@yield('title')</title>
        <meta name="_token" content="{{ csrf_token() }}"/>
        @include('include-css')
    </head>
    <!--begin::Body-->
    <body id="kt_body" class="header-fixed subheader-enabled page-loading">                        
        @yield('header')        
        @yield('content')        
        @yield('footer')        
        @include('include-js')
    </body>
</html>