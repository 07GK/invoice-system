@extends('layout')
@section('title', 'Invoice')
<!--begin::Main-->
<div class="d-flex flex-column flex-root">
    <!--begin::Page-->
    <div class="d-flex flex-row flex-column-fluid page">
        <!--begin::Wrapper-->
        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
            @section('header')
            @include('header')
            @stop
            @section('content')
            <div class="d-flex flex-row flex-column-fluid container">
                <!--begin::Content Wrapper-->
                <div class="main d-flex flex-column flex-row-fluid">
                    <!--begin::Subheader-->
                    <div class="subheader py-2 py-lg-4" id="kt_subheader">
                        <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                            <!--begin::Info-->
                            <div class="d-flex align-items-center flex-wrap mr-1">
                                <!--begin::Page Heading-->
                                <div class="d-flex align-items-baseline mr-5">
                                    <!--begin::Page Title-->
                                    <h5 class="text-dark font-weight-bold my-2 mr-5">Manage Invoice</h5>
                                    <!--end::Page Title-->
                                    <!--begin::Breadcrumb-->
                                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                                        <li class="breadcrumb-item">
                                            <a href="{{ url('projects/') }}" class="text-muted">View</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="" class="text-muted">Add</a>
                                        </li>
                                    </ul>
                                    <!--end::Breadcrumb-->
                                </div>
                                <!--end::Page Heading-->
                            </div>
                            <!--end::Info-->
                        </div>
                    </div>
                    <!--end::Subheader-->
                    <div class="content flex-column-fluid" id="kt_content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-custom gutter-b example example-compact">
                                    <div class="card-header">
                                        <h3 class="card-title">Add Invoice</h3>
                                    </div>
                                    <!--begin::Form-->
                                    <form action="/invoice/save" id="invoice_form" class="form-submit-event" method="POST">

                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group mb-2 col-4 ml-auto">
                                                        <label>Invoice</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">#</span>
                                                            </div>
                                                            <input type="text" class="form-control" placeholder="" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 row">
                                                    <div class="form-group col-6">
                                                        <label for="client_id">Select Client</label>
                                                        <select name="client_id" class="form-control singleSelect" id="client_id" data-placeholder="Search and select clients">
                                                            <option></option>
                                                            @forelse ($clientData as $row)
                                                            <option value="{{ $row->id }}" data-currency="{{ $row->currency }}">{{$row->name .' - '. $row->email }}</option>
                                                            @empty
                                                            <option>No clients available</option>
                                                            @endforelse
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-3 offset-md-3">
                                                        <label for="invoice_date">Invocie Date</label>
                                                        <input type="date" class="form-control" name="invoice_date">
                                                    </div>
                                                    <div class="form-group col-3 offset-md-9">
                                                        <label for="due_date">Due Date</label>
                                                        <input type="date" class="form-control" name="due_date">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="separator separator-dashed my-8"></div>
                                            <div class="project_details d-none">
                                                <div class="row text-center">
                                                    <div class="form-group col-2">
                                                        <p class="bg-secondary py-2 px-4">Select Project</p>
                                                    </div>
                                                    <div class="form-group col-2">
                                                        <p class="bg-secondary py-2 px-4">Project Description</p>
                                                    </div>
                                                    <div class="form-group col-2">
                                                        <p class="bg-secondary py-2 px-4">Is hourly based</p>
                                                    </div>
                                                    <div class="form-group col-2">
                                                        <p class="bg-secondary py-2 px-4">Rate per hour</p>
                                                    </div>
                                                    <div class="form-group col-2">
                                                        <p class="bg-secondary py-2 px-4">Total Hours</p>
                                                    </div>
                                                    <div class="form-group col-2">
                                                        <p class="bg-secondary py-2">Total Amount <span class='currency' class="small"></span> </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="summary container d-none">
                                            <div class="row text-center offset-md-6">
                                                <div class="col-12" id="tax_details">
                                                    <div class="row text-center">
                                                        <div class="form-group col-4">

                                                        </div>
                                                        <div class="form-group col-4">
                                                            <h6 class="py-2 px-4">Subtotal</h6>
                                                        </div>
                                                        <div class="form-group col-4">
                                                            <p class="py-2 px-4 subtotal">0</p>
                                                        </div>
                                                    </div>
                                                    <div class="row text-center">
                                                        <div class="form-group col-12">
                                                            <h6 class="py-2 px-4">Tax Details</h6>
                                                        </div>
                                                        <div class="form-group col-4">
                                                            <h6 class="py-2 px-4">Tax Name</h6>
                                                        </div>
                                                        <div class="form-group col-4">
                                                            <h6 class="py-2 px-4">Tax Percentage</h6>
                                                        </div>
                                                        <div class="form-group col-4">
                                                            <h6 class="py-2 px-4">Tax Amount</h6>
                                                        </div>
                                                    </div>
                                                </div>                                                
                                                <div class="form-group col-12 text-right">
                                                    <a href="javascript:;" class="fomr-control btn btn-danger font-weight-bolder add_more_tax">Add more tax</a>
                                                </div>                                                
                                                <div class=" col-12 row text-center offset-md-4">
                                                    <div class="form-group col-4">
                                                        <h6 class="py-2 px-4">Total <span class='currency' class="small"></span> </h6>
                                                    </div>
                                                    <div class="form-group col-4">
                                                        <h4 class="py-2 px-4 nettotal text-primary">0</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary mr-2 submit">Download</button>
                                            <button type="reset" class="btn btn-secondary">Cancel</button>
                                        </div>
                                </div>
                                </form>
                                <!--end::Form-->
                                <div class="row d-none" id="project_user_input_data">
                                    <div class="form-group col-2">
                                        <select name="project_ids[]" class="form-control project_ids">
                                        </select>
                                    </div>
                                    <div class="form-group col-2">
                                        <input type="text" class="form-control project_desc" name="project_description[]">
                                    </div>
                                    <div class="form-group col-2">
                                        <select class="form-control is_hourly_based" name="is_hourly_based[]">
                                            <option value="1">YES</option>
                                            <option value="0">NO</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-2">
                                        <input type="text" class="form-control rate_per_hour" name="rate_per_hour[]" readonly placeholder="Enter Rate Per Hour">
                                    </div>
                                    <div class="form-group col-2">
                                        <input type="text" class="form-control total_hours" name="total_hours[]" readonly placeholder="Enter Total Hours">
                                    </div>
                                    <div class="form-group col-2">
                                        <input type="number" class="form-control total" name="total[]" placeholder="Enter Total Amount" readonly>
                                    </div>
                                </div>
                                <div class="row d-none" id="add_more">
                                    <div class="form-group col-2">
                                        <a href="javascript:;" class="fomr-control btn btn-danger font-weight-bolder add_more">Add more</a>
                                    </div>
                                </div>
                                <div id="tax_user_input" class="d-none">
                                    <div class="row">
                                        <div class="form-group col-4">
                                            <input type="hidden" class="tax_id" name="tax_id" value="">                                            
                                            <input type="text" name="tax_name[]" class="tax_name form-control" placeholder="Enter Tax Name">
                                        </div>
                                        <div class="form-group col-4">
                                            <input type="number" class="tax_percentage form-control" name="tax_percentage[]" placeholder="Enter Tax Percentage">
                                        </div>
                                        <div class="form-group col-4">
                                            <p class="py-2 px-4 tax_amount">0</p>
                                            <input type="hidden" name="tax_amount[]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Content-->
            </div>
            <!--begin::Content Wrapper-->
        </div>
    </div>
    @stop
    @section('footer')
    @include('footer')
    @stop