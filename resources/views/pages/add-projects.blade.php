
@extends('layout')
@section('title', trans("messages.add_projects").' | '.trans("messages.manage_projects") )
<!--begin::Main-->
<div class="d-flex flex-column flex-root">
    <!--begin::Page-->
    <div class="d-flex flex-row flex-column-fluid page">
        <!--begin::Wrapper-->
        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
            @section('header')
            @include('header')
            @stop
            @section('content')
            <div class="d-flex flex-row flex-column-fluid container">
                <!--begin::Content Wrapper-->
                <div class="main d-flex flex-column flex-row-fluid">
                    <!--begin::Subheader-->
                    <div class="subheader py-2 py-lg-4" id="kt_subheader">
                        <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                            <!--begin::Info-->
                            <div class="d-flex align-items-center flex-wrap mr-1">
                                <!--begin::Page Heading-->
                                <div class="d-flex align-items-baseline mr-5">
                                    <!--begin::Page Title-->
                                    <h5 class="text-dark font-weight-bold my-2 mr-5">{{ LANG::get('messages.manage_projects') }}</h5>
                                    <!--end::Page Title-->
                                    <!--begin::Breadcrumb-->
                                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                                        <li class="breadcrumb-item">
                                            <a href="{{ url('projects/') }}" class="text-muted">{{ LANG::get('messages.view') }}</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="" class="text-muted">{{ LANG::get('messages.add') }}</a>
                                        </li>
                                    </ul>
                                    <!--end::Breadcrumb-->
                                </div>
                                <!--end::Page Heading-->
                            </div>
                            <!--end::Info-->                          
                        </div>
                    </div>
                    <!--end::Subheader-->
                    <div class="content flex-column-fluid" id="kt_content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-custom gutter-b example example-compact">
                                    <div class="card-header">
                                        <h3 class="card-title">{{ LANG::get('messages.add_projects') }}</h3>
                                    </div>
                                    <!--begin::Form-->
                                    <form action="/projects" id="project_form" class="form-submit-event" method="POST">
                                        @csrf
                                        <div class="card-body row">
                                            <div class="form-group col-12">
                                                <label for="client_id">Select Client *</label>
                                                <select name="client_id" class="form-control @error('client_id') error @enderror singleSelect" >                                                    
                                                    @forelse ($clientData as $row)
                                                        <option value="{{ $row->id }}" {{ ( old('client_id') == $row->id ) ? 'selected' : '' }} >{{$row->name}}</option>
                                                        @empty
                                                        <option value="">No clients available</option>
                                                    @endforelse
                                                </select>   
                                                @error('client_id')
                                                    <label id="name-error" class="error" for="client_id">{{ 'This field is required' }}</label>
                                                @enderror
                                            </div>
                                            <div class="form-group col-12">
                                                <label for="name">Project Name *</label>
                                                <input type="name" class="form-control @error('name') error @enderror" name="name" placeholder="Enter Project Name" value="{{ old('name') }}" >
                                                @error('name')
                                                    <label id="name-error" class="error" for="name">{{ 'This field is required' }}</label>
                                                @enderror
                                            </div>
                                            <div class="form-group col-12">
                                                <label for="desc"> Project Description *</label>
                                                <input type="text" class="form-control @error('desc') error @enderror" name="desc" placeholder="Enter Project Description" value="{{ old('desc') }}" >
                                                @error('desc')
                                                    <label id="name-error" class="error" for="desc">{{ 'This field is required' }}</label>
                                                @enderror
                                            </div>
                                            <div class="form-group col-2 my-auto">
                                                <label> Is Hourly Based ?</label>
                                                <div class="radio-inline">															                                                    
                                                    <span class="switch switch-outline switch-icon switch-primary">
                                                        <label>
                                                            <input type="checkbox" name="is_hourly_based" class="radio is_hourly_based" {{ ( array_key_exists('is_hourly_based',old()) ) ? 'checked' : '' }}>
                                                            <span></span>
                                                        </label>
                                                    </span>                                                    
                                                </div>                                                
                                            </div>
                                            <div class="form-group col-12 {{ ( array_key_exists('is_hourly_based',old()) ) ? '' : 'd-none' }} rate_per_hour">
                                                <label for="rate_per_hour"> Rate per hour *</label>
                                                <input type="number" class="form-control @error('rate_per_hour') error @enderror" name="rate_per_hour" placeholder="Enter Rate Per Hour" value="{{ old('rate_per_hour') }}">
                                                @error('rate_per_hour')
                                                    <label id="name-error" class="error" for="desc">{{ 'This field is required' }}</label>
                                                @enderror
                                            </div>                                            
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary mr-2 submit">{{ LANG::get('messages.add') }}</button>
                                            <button type="reset" class="btn btn-secondary">{{ LANG::get('messages.reset') }}</button>
                                        </div>
                                </div>
                                </form>
                                <!--end::Form-->
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Content-->
            </div>
            <!--begin::Content Wrapper-->
        </div>
    </div>
    @stop
    @section('footer')
    @include('footer')
    @stop