@extends('layout')
@section('title', trans("messages.view_clients").' | '.trans("messages.manage_clients") )
<!--begin::Main-->
<div class="d-flex flex-column flex-root">
    <!--begin::Page-->
    <div class="d-flex flex-row flex-column-fluid page">
        <!--begin::Wrapper-->
        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
            @section('header')
            @include('header')
            @stop
            @section('content')
            <!--begin::Card-->
            <div class="d-flex flex-row flex-column-fluid container">
            <div class="main d-flex flex-column flex-row-fluid">
            <div class="subheader py-2 py-lg-4" id="kt_subheader">
                <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                    <!--begin::Info-->
                    <div class="d-flex align-items-center flex-wrap mr-1">
                        <!--begin::Page Heading-->
                        <div class="d-flex align-items-baseline mr-5">
                            <!--begin::Page Title-->
                            <h5 class="text-dark font-weight-bold my-2 mr-5">{{ LANG::get('messages.manage_clients') }}</h5>
                            <!--end::Page Title-->
                            <!--begin::Breadcrumb-->
                            <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('/clients') }}" class="text-muted">{{ LANG::get('messages.view') }}</a>
                                </li>                               
                            </ul>
                            <!--end::Breadcrumb-->
                        </div>
                        <!--end::Page Heading-->
                    </div>
                    <!--end::Info-->                      
                </div>
            </div>
            <div class="content flex-column-fluid" id="kt_content">

            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap py-3">
                    <div class="card-title">
                        <h3 class="card-label"> {{ LANG::get('messages.view_clients') }} </h3>
                    </div>
                    <div class="card-toolbar">                        
                        <!--begin::Button-->
                        <a href="{{ url('clients?flag=deleted') }}" class="btn btn-danger font-weight-bolder mr-2">
                        <i class="icon-1x text-white flaticon-delete"></i>    
                        {{ Lang::get('messages.deleted_clients') }}</a>
                        <a href="{{ url('clients/add') }}" class="btn btn-primary font-weight-bolder">
                            <span class="svg-icon svg-icon-md">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <circle fill="#000000" cx="9" cy="15" r="6" />
                                        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>{{ Lang::get('messages.new_records') }}</a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->                    
                    <table class="table table-bordered table-checkable datatable" data-url="{{ url('clients/view?flag=').app('request')->input('flag') }}">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone no</th>
                                <th>Address</th>
                                <th>GSTIN</th>
                                <th>Place of supply</th>
                                <th>Currency</th>
                                <th>State code</th>                                                                
                                <th data-orderable='false'>Action</th>                                                                
                            </tr>
                        </thead>
                        <tbody>                           
                        </tbody>
                    </table>
                    <!--end: Datatable-->
                </div>
                </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('footer')
@include('footer')
@stop