

@extends('layout')
@section('title', 'Company')
<!--begin::Main-->
<div class="d-flex flex-column flex-root">
    <!--begin::Page-->
    <div class="d-flex flex-row flex-column-fluid page">
        <!--begin::Wrapper-->
        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
            @section('header')
            @include('header')
            @stop
            @section('content')
            <div class="d-flex flex-row flex-column-fluid container">
                <!--begin::Content Wrapper-->
                <div class="main d-flex flex-column flex-row-fluid">
                    <!--begin::Subheader-->
                    <div class="subheader py-2 py-lg-4" id="kt_subheader">
                        <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                            <!--begin::Info-->
                            <div class="d-flex align-items-center flex-wrap mr-1">
                                <!--begin::Page Heading-->
                                <div class="d-flex align-items-baseline mr-5">
                                    <!--begin::Page Title-->
                                    <h5 class="text-dark font-weight-bold my-2 mr-5">Manage Profile</h5>
                                    <!--end::Page Title-->
                                    <!--begin::Breadcrumb-->
                                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                                        <li class="breadcrumb-item">
                                            <a href="" class="text-muted">View</a>
                                        </li>
                                    </ul>
                                    <!--end::Breadcrumb-->
                                </div>
                                <!--end::Page Heading-->
                            </div>
                            <!--end::Info-->                         
                        </div>
                    </div>
                    <!--end::Subheader-->
                    <div class="content flex-column-fluid" id="kt_content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-custom gutter-b example example-compact">
                                    <div class="card-header">
                                        <h3 class="card-title">Company Details</h3>
                                    </div>
                                    <!--begin::Form-->
                                    <form action="/company/update" id="company_form" class="form-submit-event" method="POST">
                                        @method('PUT')
                                        @csrf
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="form-group col-4">
                                                    <label for="first_name">First Name</label>
                                                    <input type="name" class="form-control" name="first_name" placeholder="Enter First Name" value="{{ isset($company_details['first_name']) ? $company_details['first_name'] :'' }}" >
                                                </div>
                                                <div class="form-group col-4">
                                                    <label for="last_name"> Last Name</label>
                                                    <input type="text" class="form-control" name="last_name" placeholder="Enter Last Name" value="{{ isset($company_details['last_name']) ? $company_details['last_name'] : '' }}" >
                                                </div>
                                                <div class="form-group col-4">
                                                    <label for="full_title"> Full title</label>
                                                    <input type="text" class="form-control" name="full_title" placeholder="Enter Title" value="{{ isset($company_details['full_title']) ? $company_details['full_title'] : '' }}">
                                                </div>
                                                <div class="form-group col-4 ">
                                                    <label for="website_address"> Web Address </label>
                                                    <input type="text" class="form-control" name="website_address" placeholder="Enter web address" value="{{ isset($company_details['website_address']) ? $company_details['website_address'] : '' }}">
                                                </div>
                                                <div class="form-group col-4 ">
                                                    <label for="mail_address"> Mail Address </label>
                                                    <input type="email" class="form-control" name="mail_address" placeholder="Enter web address" value="{{ isset($company_details['mail_address']) ? $company_details['mail_address'] : '' }}">
                                                </div>
                                                <div class="form-group col-4 ">
                                                    <label for="company_address"> Company Address </label>
                                                    <input type="text" class="form-control" name="company_address" placeholder="Enter Company address" value="{{ isset($company_details['company_address']) ? $company_details['company_address'] : ''  }}">
                                                </div>
                                                <div class="form-group col-4 ">
                                                    <label for="cin_number"> CIN Number </label>
                                                    <input type="text" class="form-control" name="cin_number" placeholder="Enter CIN Number" value="{{ isset($company_details['company_address']) ? $company_details['cin_number'] : '' }}">
                                                </div>
                                                <div class="form-group col-4 ">
                                                    <label for="pan_number"> PAN Number </label>
                                                    <input type="text" class="form-control" name="pan_number" placeholder="Enter PAN Number" value="{{ isset($company_details['pan_number']) ? $company_details['pan_number'] : '' }}">
                                                </div>
                                                <div class="form-group col-4 ">
                                                    <label for="gst_number"> GST Number </label>
                                                    <input type="text" class="form-control" name="gst_number" placeholder="Enter GST Number" value="{{ isset($company_details['gst_number']) ? $company_details['gst_number'] : '' }}" >
                                                </div>
                                                <div class="form-group col-4 ">
                                                    <label for="iec_number"> IEC Number </label>
                                                    <input type="text" class="form-control" name="iec_number" placeholder="Enter IEC Number" value="{{ isset($company_details['iec_number']) ? $company_details['iec_number'] : '' }}" >
                                                </div>
                                                <div class="form-group col-4 ">
                                                    <label for="mobile_number"> Mobile Number </label>
                                                    <input type="number" class="form-control" name="mobile_number" placeholder="Enter Mobile Number" value="{{ isset($company_details['mobile_number']) ? $company_details['mobile_number'] : '' }}">
                                                </div>
                                            </div>
                                            <div class="card-header text-left p-0 mb-4">
                                                <h3 class="card-title">Bank Details</h3>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-4 ">
                                                    <label for="bank_name"> Bank Name </label>
                                                    <input type="text" class="form-control" name="bank_name" placeholder="Enter Bank Name" value="{{ isset($company_details['bank_name']) ? $company_details['bank_name'] : '' }}">
                                                </div>
                                                <div class="form-group col-4 ">
                                                    <label for="inr_account_number"> INR Account Number </label>
                                                    <input type="number" class="form-control" name="inr_account_number" placeholder="Enter INR Account Number" value="{{ isset($company_details['inr_account_number']) ? $company_details['inr_account_number'] : '' }}">
                                                </div>
                                                <div class="form-group col-4 ">
                                                    <label for="euro_account_number"> EURO Account Number </label>
                                                    <input type="number" class="form-control" name="euro_account_number" placeholder="Enter EURO Account Number" value="{{ isset($company_details['euro_account_number']) ? $company_details['euro_account_number'] : '' }}">
                                                </div>
                                                <div class="form-group col-4 ">
                                                    <label for="routing_number"> Routing Number </label>
                                                    <input type="text" class="form-control" name="routing_number" placeholder="Enter Routing Number" value="{{ isset($company_details['routing_number']) ? $company_details['routing_number'] : '' }}">
                                                </div>
                                                <div class="form-group col-4 ">
                                                    <label for="swift_code"> Swift Code </label>
                                                    <input type="text" class="form-control" name="swift_code" placeholder="Enter Swift Code" value="{{ isset($company_details['swift_code']) ? $company_details['swift_code'] : '' }}">
                                                </div>
                                            </div>
                                            <div class="card-header text-left p-0 mb-4">
                                                <h3 class="card-title">Other Details</h3>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-4 ">
                                                    <label for="note"> Note 1 </label>
                                                    <input type="text" class="form-control" name="note_1" placeholder="Enter Note 1"  value="{{ isset($company_details['note_1']) ? $company_details['note_1']  : '' }}">
                                                </div>
                                                <div class="form-group col-4 ">
                                                    <label for="note"> Note 2</label>
                                                    <input type="text" class="form-control" name="note_2" placeholder="Enter Note 2"  value="{{ isset($company_details['note_2']) ? $company_details['note_2'] : '' }}">
                                                </div>
                                                <div class="form-group col-4 ">
                                                    <label for="note"> Note 3</label>
                                                    <input type="text" class="form-control" name="note_3" placeholder="Enter Note 3"  value="{{ isset($company_details['note_3']) ? $company_details['note_3'] : '' }}">
                                                </div>
                                                <div class="form-group col-12 ">
                                                    <label for="note"> Thank you message</label>
                                                    <input type="text" class="form-control" name="thank_you_message" placeholder="Enter Thank You Message" value="{{ isset($company_details['thank_you_message']) ? $company_details['thank_you_message'] : '' }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary mr-2 submit">Update</button>
                                            <button type="reset" class="btn btn-secondary">Cancel</button>
                                        </div>
                                </div>
                                </form>
                                <!--end::Form-->
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Content-->
            </div>
            <!--begin::Content Wrapper-->
        </div>
    </div>
    @stop
    @section('footer')
    @include('footer')
    @stop