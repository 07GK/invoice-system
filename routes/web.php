<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth;
use App\Http\Controllers\Client;
use App\Http\Controllers\Projects;
use App\Http\Controllers\Company;
use App\Http\Controllers\Invoices;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login',function(){
    return view('pages.login');
})->middleware('isLoggedIn');

Route::post('/login', [Auth::class,'login']);
Route::post('/delete', [Auth::class,'delete']);
Route::post('/restore', [Auth::class,'restore']);
Route::get('/logout', [Auth::class,'logout']);

Route::get('/')->middleware('isLoggedIn');   
Route::get('/company',[Company::class,'index'])->middleware('isLoggedIn');   

Route::prefix('clients')->middleware('isLoggedIn')->group(function(){
    
    Route::get('/', function () {
        return view('pages.view-clients');
    });
    Route::get('/add',function(){
        return view('pages.add-clients');
    });
    Route::post('/insert',[Client::class,'insert']);
    Route::post('/get_projects_by_client_id',[Client::class,'get_projects_by_client_id']);
    Route::post('/update',[Client::class,'update']);
    Route::get('/view',[Client::class,'view']);
    Route::get('/edit/{id}',[Client::class,'edit']);
});

Route::get('/projects/view', [Projects::class,'view'])->middleware('isLoggedIn');
Route::resource('projects', Projects::class)->middleware('isLoggedIn');
Route::put('company/update', [Company::class,'update'])->middleware('isLoggedIn');

Route::get('/invoice', function(){
    return view('pages.view-invoice');
})->middleware('isLoggedIn');

Route::post('/invoice/download', [Invoices::class,'download'])->middleware('isLoggedIn');
Route::post('/invoice/save', [Invoices::class,'save'])->middleware('isLoggedIn');
// Route::post('/invoice/edit', [Invoices::class,'save'])->middleware('isLoggedIn');
Route::get('/invoice/view', [Invoices::class,'view'])->middleware('isLoggedIn');
