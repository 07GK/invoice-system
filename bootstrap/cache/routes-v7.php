<?php

/*
|--------------------------------------------------------------------------
| Load The Cached Routes
|--------------------------------------------------------------------------
|
| Here we will decode and unserialize the RouteCollection instance that
| holds all of the route information for an application. This allows
| us to instantaneously load the entire route map into the router.
|
*/

app('router')->setCompiledRoutes(
    array (
  'compiled' => 
  array (
    0 => false,
    1 => 
    array (
      '/api/user' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::w7dRUqrjAbNMSu4c',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/login' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::3FYHYzwlm9l5MBEd',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::xIY5Gbx05NqRmEs9',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/delete' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::OnInM4uwB5oNP8zL',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/restore' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::mhH380LhJeTJnYy7',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/logout' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::oWeoPezKF9A8EIV8',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::YLrJdk0E0YzwI5m4',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/company' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::DFqCGOVjbEsM20t5',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/clients' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::KpqHUIDEVXc33eVw',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/clients/add' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::ztSXqLMBrf9KTFUG',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/clients/insert' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::aOeQkz016ObkLNWY',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/clients/get_projects_by_client_id' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::hNT3GgE8SFzDOe3b',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/clients/update' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::vDlvqWc90BNHXpPs',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/clients/view' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::4ydzzDqpHGCU98md',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/projects/view' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::ClTLz0EH5eLzZ8xq',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/projects' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'projects.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'projects.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/projects/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'projects.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/company/update' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::RGmyr441pn7HpRf4',
          ),
          1 => NULL,
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/invoice' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::0VLbXtoAZzryqPN0',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/invoice/download' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::0OY8kbMVYbLyCwdz',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/invoice/save' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::JAVjaMikQ2CXoQW9',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/invoice/view' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::ZuoC52wl46FmAgjY',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
    ),
    2 => 
    array (
      0 => '{^(?|/clients/edit/([^/]++)(*:29)|/projects/([^/]++)(?|(*:57)|/edit(*:69)|(*:76)))/?$}sDu',
    ),
    3 => 
    array (
      29 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::WaVKtDOkl8GVp9ah',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      57 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'projects.show',
          ),
          1 => 
          array (
            0 => 'project',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      69 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'projects.edit',
          ),
          1 => 
          array (
            0 => 'project',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      76 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'projects.update',
          ),
          1 => 
          array (
            0 => 'project',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'projects.destroy',
          ),
          1 => 
          array (
            0 => 'project',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        2 => 
        array (
          0 => NULL,
          1 => NULL,
          2 => NULL,
          3 => NULL,
          4 => false,
          5 => false,
          6 => 0,
        ),
      ),
    ),
    4 => NULL,
  ),
  'attributes' => 
  array (
    'generated::w7dRUqrjAbNMSu4c' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/user',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'api',
          1 => 'auth:api',
        ),
        'uses' => 'C:32:"Opis\\Closure\\SerializableClosure":289:{@JSg+ui288h2d6rDqdk05Yu7PrpLM346U5I0NFo93Ulo=.a:5:{s:3:"use";a:0:{}s:8:"function";s:77:"function (\\Illuminate\\Http\\Request $request) {
    return $request->user();
}";s:5:"scope";s:37:"Illuminate\\Routing\\RouteFileRegistrar";s:4:"this";N;s:4:"self";s:32:"00000000062bc6630000000036b38568";}}',
        'namespace' => NULL,
        'prefix' => 'api',
        'where' => 
        array (
        ),
        'as' => 'generated::w7dRUqrjAbNMSu4c',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::3FYHYzwlm9l5MBEd' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'login',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'uses' => 'C:32:"Opis\\Closure\\SerializableClosure":258:{@GGrNOEyJbDBD72+ViDMR9ka5YN3Uo9qJRPX9vhfEEso=.a:5:{s:3:"use";a:0:{}s:8:"function";s:46:"function(){
    return \\view(\'pages.login\');
}";s:5:"scope";s:37:"Illuminate\\Routing\\RouteFileRegistrar";s:4:"this";N;s:4:"self";s:32:"00000000062bc65f0000000036b38568";}}',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::3FYHYzwlm9l5MBEd',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::xIY5Gbx05NqRmEs9' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'login',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Auth@login',
        'controller' => 'App\\Http\\Controllers\\Auth@login',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::xIY5Gbx05NqRmEs9',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::OnInM4uwB5oNP8zL' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'delete',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Auth@delete',
        'controller' => 'App\\Http\\Controllers\\Auth@delete',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::OnInM4uwB5oNP8zL',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::mhH380LhJeTJnYy7' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'restore',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Auth@restore',
        'controller' => 'App\\Http\\Controllers\\Auth@restore',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::mhH380LhJeTJnYy7',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::oWeoPezKF9A8EIV8' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'logout',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Auth@logout',
        'controller' => 'App\\Http\\Controllers\\Auth@logout',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::oWeoPezKF9A8EIV8',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::YLrJdk0E0YzwI5m4' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => '/',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'uses' => 'C:32:"Opis\\Closure\\SerializableClosure":334:{@8BrTupz4nsPUJoB0bidqaY6YAdUdWL5YYBWwlfoMi/4=.a:5:{s:3:"use";a:1:{s:3:"uri";s:1:"/";}s:8:"function";s:110:"function () use ($uri) {
            throw new \\LogicException("Route for [{$uri}] has no action.");
        }";s:5:"scope";s:30:"Illuminate\\Routing\\RouteAction";s:4:"this";N;s:4:"self";s:32:"00000000062bc6640000000036b38568";}}',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::YLrJdk0E0YzwI5m4',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::DFqCGOVjbEsM20t5' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'company',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'uses' => 'App\\Http\\Controllers\\Company@index',
        'controller' => 'App\\Http\\Controllers\\Company@index',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::DFqCGOVjbEsM20t5',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::KpqHUIDEVXc33eVw' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'clients',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'uses' => 'C:32:"Opis\\Closure\\SerializableClosure":275:{@j9/B8R5azc05KeVUsyf5qS4vbDgXr0hBcgxX86YCtSc=.a:5:{s:3:"use";a:0:{}s:8:"function";s:63:"function () {
        return \\view(\'pages.view-clients\');
    }";s:5:"scope";s:37:"Illuminate\\Routing\\RouteFileRegistrar";s:4:"this";N;s:4:"self";s:32:"00000000062bc6680000000036b38568";}}',
        'namespace' => NULL,
        'prefix' => '/clients',
        'where' => 
        array (
        ),
        'as' => 'generated::KpqHUIDEVXc33eVw',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::ztSXqLMBrf9KTFUG' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'clients/add',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'uses' => 'C:32:"Opis\\Closure\\SerializableClosure":272:{@t1cIHtWRrU2RlNT5birOZUKP8TPpqZThdiccG3tgzAk=.a:5:{s:3:"use";a:0:{}s:8:"function";s:60:"function(){
        return \\view(\'pages.add-clients\');
    }";s:5:"scope";s:37:"Illuminate\\Routing\\RouteFileRegistrar";s:4:"this";N;s:4:"self";s:32:"00000000062bc66e0000000036b38568";}}',
        'namespace' => NULL,
        'prefix' => '/clients',
        'where' => 
        array (
        ),
        'as' => 'generated::ztSXqLMBrf9KTFUG',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::aOeQkz016ObkLNWY' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'clients/insert',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'uses' => 'App\\Http\\Controllers\\Client@insert',
        'controller' => 'App\\Http\\Controllers\\Client@insert',
        'namespace' => NULL,
        'prefix' => '/clients',
        'where' => 
        array (
        ),
        'as' => 'generated::aOeQkz016ObkLNWY',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::hNT3GgE8SFzDOe3b' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'clients/get_projects_by_client_id',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'uses' => 'App\\Http\\Controllers\\Client@get_projects_by_client_id',
        'controller' => 'App\\Http\\Controllers\\Client@get_projects_by_client_id',
        'namespace' => NULL,
        'prefix' => '/clients',
        'where' => 
        array (
        ),
        'as' => 'generated::hNT3GgE8SFzDOe3b',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::vDlvqWc90BNHXpPs' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'clients/update',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'uses' => 'App\\Http\\Controllers\\Client@update',
        'controller' => 'App\\Http\\Controllers\\Client@update',
        'namespace' => NULL,
        'prefix' => '/clients',
        'where' => 
        array (
        ),
        'as' => 'generated::vDlvqWc90BNHXpPs',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::4ydzzDqpHGCU98md' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'clients/view',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'uses' => 'App\\Http\\Controllers\\Client@view',
        'controller' => 'App\\Http\\Controllers\\Client@view',
        'namespace' => NULL,
        'prefix' => '/clients',
        'where' => 
        array (
        ),
        'as' => 'generated::4ydzzDqpHGCU98md',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::WaVKtDOkl8GVp9ah' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'clients/edit/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'uses' => 'App\\Http\\Controllers\\Client@edit',
        'controller' => 'App\\Http\\Controllers\\Client@edit',
        'namespace' => NULL,
        'prefix' => '/clients',
        'where' => 
        array (
        ),
        'as' => 'generated::WaVKtDOkl8GVp9ah',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::ClTLz0EH5eLzZ8xq' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'projects/view',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'uses' => 'App\\Http\\Controllers\\Projects@view',
        'controller' => 'App\\Http\\Controllers\\Projects@view',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::ClTLz0EH5eLzZ8xq',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'projects.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'projects',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'as' => 'projects.index',
        'uses' => 'App\\Http\\Controllers\\Projects@index',
        'controller' => 'App\\Http\\Controllers\\Projects@index',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'projects.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'projects/create',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'as' => 'projects.create',
        'uses' => 'App\\Http\\Controllers\\Projects@create',
        'controller' => 'App\\Http\\Controllers\\Projects@create',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'projects.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'projects',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'as' => 'projects.store',
        'uses' => 'App\\Http\\Controllers\\Projects@store',
        'controller' => 'App\\Http\\Controllers\\Projects@store',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'projects.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'projects/{project}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'as' => 'projects.show',
        'uses' => 'App\\Http\\Controllers\\Projects@show',
        'controller' => 'App\\Http\\Controllers\\Projects@show',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'projects.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'projects/{project}/edit',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'as' => 'projects.edit',
        'uses' => 'App\\Http\\Controllers\\Projects@edit',
        'controller' => 'App\\Http\\Controllers\\Projects@edit',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'projects.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'projects/{project}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'as' => 'projects.update',
        'uses' => 'App\\Http\\Controllers\\Projects@update',
        'controller' => 'App\\Http\\Controllers\\Projects@update',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'projects.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'projects/{project}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'as' => 'projects.destroy',
        'uses' => 'App\\Http\\Controllers\\Projects@destroy',
        'controller' => 'App\\Http\\Controllers\\Projects@destroy',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::RGmyr441pn7HpRf4' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'company/update',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'uses' => 'App\\Http\\Controllers\\Company@update',
        'controller' => 'App\\Http\\Controllers\\Company@update',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::RGmyr441pn7HpRf4',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::0VLbXtoAZzryqPN0' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'invoice',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'uses' => 'C:32:"Opis\\Closure\\SerializableClosure":265:{@jZswF1UxTr2Yq2NfQ7RB0jXtpnkO5uO+4wAQdEuzPMs=.a:5:{s:3:"use";a:0:{}s:8:"function";s:53:"function(){
    return \\view(\'pages.view-invoice\');
}";s:5:"scope";s:37:"Illuminate\\Routing\\RouteFileRegistrar";s:4:"this";N;s:4:"self";s:32:"00000000062bc6690000000036b38568";}}',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::0VLbXtoAZzryqPN0',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::0OY8kbMVYbLyCwdz' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'invoice/download',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'uses' => 'App\\Http\\Controllers\\Invoices@download',
        'controller' => 'App\\Http\\Controllers\\Invoices@download',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::0OY8kbMVYbLyCwdz',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::JAVjaMikQ2CXoQW9' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'invoice/save',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'uses' => 'App\\Http\\Controllers\\Invoices@save',
        'controller' => 'App\\Http\\Controllers\\Invoices@save',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::JAVjaMikQ2CXoQW9',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::ZuoC52wl46FmAgjY' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'invoice/view',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'isLoggedIn',
        ),
        'uses' => 'App\\Http\\Controllers\\Invoices@view',
        'controller' => 'App\\Http\\Controllers\\Invoices@view',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::ZuoC52wl46FmAgjY',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
  ),
)
);
