<?php return array (
  'elibyy/tcpdf-laravel' => 
  array (
    'providers' => 
    array (
      0 => 'Elibyy\\TCPDF\\ServiceProvider',
    ),
    'aliases' => 
    array (
      'PDF' => 'Elibyy\\TCPDF\\Facades\\TCPDF',
    ),
  ),
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'fruitcake/laravel-cors' => 
  array (
    'providers' => 
    array (
      0 => 'Fruitcake\\Cors\\CorsServiceProvider',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
);