
// var url = window.location.origin + window.location.pathname;
// var $selector = $('#kt_header_menu a[href="' + url + '"]');
// $($selector).closest('li').addClass('menu-item-here');

function submit(form){           
    var formData = new FormData(form);   
    $.ajax({
        type: 'POST',
        url: $(form).attr('action'),
        data: formData,       
        // cache: false,
        // contentType: false,
        // processData: false,
        dataType: 'json',
        success: function(result) {            
            // if(result.error){
            //     Toast.fire({icon: 'error',title: result.message});
            // }else{                            
            //     window.location.href = result.url;                                
            // }            
        }
    });   
}

$(document).on('click','.add_tax,.remove_tax',function(){ 
    if($(this).hasClass('add_tax')){
        var row = $(this).closest('.row').clone();
        var count = $('#tax_details_html').find('.row').length;        
        if(count > 0){
            $(row).find('.action > a').removeClass('add_tax').addClass('remove_tax').find('i').removeClass('far fa-plus-square').addClass('fas fa-times').closest('.row').find('input').val('').removeClass('error').next('label').remove();            
            $(row).find('input[name="tax_name[0]"]').attr('name','tax_name['+ count +']')
            $(row).find('input[name="tax_percentage[0]"]').attr('name','tax_percentage['+ count  +']');
            $('#tax_details_html').append(row).find('input[name="tax_name['+count+']"]').rules('add',{
                required:true });
            $('#tax_details_html').find('.row:last').find('input[name="tax_percentage['+count+']"]').rules('add',{
                required:true });

        }
    }else{
        $(this).closest('.row').remove();
    }
});


$(document).ready( function () {
    $('.datatable').DataTable({
        'processing': true,
        'serverSide': true,
        "bInfo": false,
        "order":[[0,"desc"]],
        'serverMethod': 'get',
        'ajax': {
            'url':$(this).find('table').data('url'),
            "data": function ( d ) {            
                d.client_id = ($('#client_id').val()) ? $('#client_id').val(): '';                
            }
        }      
     });
} );

$(document).on('change','#client_id',function(){
    $('.dataTable').DataTable().ajax.reload();   
});

$('.singleSelect').select2();

$('#client_form').validate({
    rules: {
        name: {
            required: true  
        },
        email: {
            required: true,            
        },
        phone_no: {
            required: true,                        
        },        
        address: {
            required: true
        },
        gstin: {
            required: true
        },
        place_of_supply: {
            required: true
        },
        currency:{
            required: true
        },
        state_code:{
            required: true
        },
        'tax_name[0]':{
            required: true
        },
        'tax_percentage[0]':{
            required: true
        }
    },
    submitHandler: function (form) {                  
        // $(form).submit();                                    
        submit(form);
    }
});

$('#project_form').validate({
    rules: {
        name: {
            required: true
        },
        desc: {
            required: true            
        },      
        rate_per_hour:{
            required: function(){
                return $('.is_hourly_based').is(':checked');
            },
        }
    },
    submitHandler: function (form) {        
        submit(form);                                    
    }
});

$('#company_form').validate({
    rules: {
        first_name: {
            required: true
        },
        last_name: {
            required: true,            
        },  
        full_title: {
            required: true,            
        },
        website_address:{
            required: true,            
        },
        mail_address:{
            required: true,            
        },
        company_address : {
            required: true,            
        },
        cin_number:{
            required: true,            
        },
        pan_number:{
            required: true,            
        },
        gst_number:{
            required: true,            
        },
        iec_number:{
            required: true,            
        },
        mobile_number:{
            required: true,            
        },
        bank_name:{
            required: true,            
        },
        inr_account_number:{
            required: true,            
        },
        euro_account_number:{
            required: true,            
        },
        routing_number:{
            required: true,            
        },
        swift_code:{
            required: true,            
        },
        note_1:{
            required: true,            
        },
        note_2:{
            required: true,            
        },
        note_3:{
            required: true,            
        },
        thank_you_message : {
            required: true,            
        }
    },
    submitHandler: function (form) {        
        submit(form);                                    
    }
});

$('#kt_login_signin_form').validate({
    rules: {
        email: {
            required: true
        },
        password: {
            required: true,            
        },      
    },
    submitHandler: function (form) {        
            submit(form);                                    
    }
});

$(document).ready(function(){
    $('#updateCurrencyList').val($('#updateCurrencyList').data('currency'));
});

$(document).on('click','#project_form .is_hourly_based',function(){    
    $('.rate_per_hour').toggleClass('d-none').find('input').removeClass('error').next().remove();
})


const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 1000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
});



$(document).on('change','#client_id',function(){
    var t=this;
    $.ajax({
        type: 'POST',
        url: 'clients/get_projects_by_client_id',
        data: {id: $(this).val(), "_token": $('input[name="_token"]').val() },                  
        dataType: 'json',
        success: function(result) {                        
            if(result.error){

            }else{
                var projects = result.data[0].projects;
                var taxes = result.data[0].taxes;
                $('.project_details').find('.row').not(':first').remove();
                $('#invoice_form').find('.add_more').closest('.row').remove();
                var $options = '<option value=""> Select Projects </option>';                
                for (let index = 0; index < projects.length; index++) {
                    $options += '<option value="'+projects[index].id+'" data-is-hourly-based="'+projects[index].is_hourly_based+'" data-project-desc= "'+projects[index].desc+'" data-rate-per-hour= "'+projects[index].rate_per_hour+'" >'+projects[index].name+'</option>';
                }                                                     
                $('.project_details').removeClass('d-none').append(addRow($options)).find('.currency').text( '( '+ $(t).find('option[value="'+$(t).val()+'"]').data('currency') + ' )' ).closest('.project_details').find('.project_ids').select2();                
                $('#invoice_form').find('.card-body').append('<div class="row">'+$('#add_more').html()+'</div>');
                                
                for (let index = 0; index < taxes.length; index++) {                                                        
                    // $('#tax_details').append($newTaxRow.html('<input type="hidden" name="tax_id[]" value="'+taxes[index].id+'"><p class="py-2 px-4">'+taxes[index].name+'</p>').closest('.form-group')[0]).closest('#tax_details').append($newTaxRow.html('<input type="text" name="tax_percentage" value="'+taxes[index].percentage+'">').closest('.form-group')[0]);                                        
                    var $newTaxRow = $('#tax_user_input').find('.row').clone();                    
                    $newTaxRow.find('.tax_id').val(taxes[index].id);
                    $newTaxRow.find('.tax_name').val(taxes[index].name);
                    $newTaxRow.find('.tax_percentage').val(taxes[index].percentage);
                    $('#tax_details').append($newTaxRow);
                }            
                $('.summary').removeClass('d-none');
            }
        }
    });   
});

function addRow($options){
    return '<div class="row">'+$('#project_user_input_data').clone().removeClass('d-none').find('.project_ids').html($options).closest('.row').find('.form-group').not(':first').hide().closest('.row').html()+'</div>';
}

$(document).on('click','.add_more',function(){    
    var $row = addRow($('#invoice_form').find('.project_ids').last().html());   
    $('.project_details').append($row).find('.project_ids').select2();                
    
});

$(document).on('click','.add_more_tax',function(){        
    $('#tax_details').append($('#tax_user_input').find('.row').clone());
});


$(document).on('change','.project_ids,.is_hourly_based',function(){

    var $row = $(this).closest('.row');
    var $element = $row.find('.project_ids option:selected');
    $row.find('.project_desc').val($element.data('project-desc')).parent().show();    
    var is_hourly_based = $(this).hasClass('project_ids') ? $element.data('is-hourly-based') : parseInt($(this).val()) ;                        
    $row.find('.is_hourly_based').val(  is_hourly_based ? 1 : 0 ).parent().show();   
   
   if(is_hourly_based){
    $row.find('.rate_per_hour').prop('readonly',false).val( $element.data('rate-per-hour') ).parent().show();    
    $row.find('.total').prop('readonly',true).val('0').parent().show();   
    $row.find('.total_hours').prop('readonly',false).val('').attr('data-rate-per-hour',$element.data('rate-per-hour')).parent().show();   
   }else{
    $row.find('.rate_per_hour').prop('readonly',true).val('N/A').parent().show();   
    $row.find('.total').prop('readonly',false).val('').parent().show();   
    $row.find('.total_hours').prop('readonly',true).val('N/A').parent().show();   
   }
   cal_subtotal();
});

$(document).on('keyup','.total_hours,.rate_per_hour',function(){    
    
    var $totalElement = $(this).closest('.row').find('.total');
    var $total = 0;
    if($(this).hasClass('total_hours')){
        var $rate_per_hour = $(this).closest('.row').find('.rate_per_hour').val();    
        if($.isNumeric($rate_per_hour)){                            
            $total = $rate_per_hour * $(this).val();
            $totalElement.val($total);                
        }        
    }else{
        var $total_hours = $(this).closest('.row').find('.total_hours').val();    
        if($.isNumeric($total_hours)){                            
            $total = $total_hours * $(this).val();
            $totalElement.val($total);                
        }        
    }   
    cal_subtotal();
});

$(document).on('keyup','.tax_percentage',function(){    
    var total_per = 0;
    $('.tax_percentage').each(function(){        
        if($.isNumeric($(this).val())){
            total_per += parseInt($(this).val());
        }
    });
    // console.log(total_per);
    if(total_per > 100){
        Toast.fire({icon: 'error',title: 'Total Tax less than 100 percentage can only be added'});  
        $(this).val('');
    }else{
        cal_tax(parseFloat($('.subtotal').text()));
    }
});

$(document).on('keyup','.total',function(){        
    cal_subtotal();
});

function cal_subtotal(){
    var subtotal = 0;    
    $('.project_details .total').each(function(){         
        if($.isNumeric($(this).val())){       
            console.log($(this).val(),subtotal);
            subtotal += parseInt($(this).val());            
        }
     });
     
    $('.subtotal').html(subtotal);
    cal_tax(subtotal);     
}

function cal_tax(subtotal){
    var tax_amount = [];
    $('#tax_details .tax_percentage').each(function(){        
        var tax = subtotal * ($(this).val() / 100 );
        tax_amount.push(parseFloat(tax.toFixed(2))); 
        $(this).closest('.form-group').next().find('.tax_amount').text(tax.toFixed(2)).next().val(tax.toFixed(2));                       
    });    
    var total_tax_amount = tax_amount.reduce(function(a, b){return a + b;}, 0);
    subtotal += total_tax_amount;
    $('.nettotal').html(subtotal.toFixed(2));
}

$(document).on('click','.delete,.restore',function(){
    var id = $(this).data('id');
    var table = $(this).data('table');
    if($(this).hasClass('delete')){
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'POST',
                    url: 'delete',
                    data: {id: id, table:table ,"_token": $('meta[name="_token"]').attr('content') },                                  
                    dataType: 'json',
                    success: function(result) {                        
                        if(!result.error){                        
                            Toast.fire({icon: 'success',title: result.message});  
                            $('.dataTable').DataTable().ajax.reload();                
                        }else{
                            Toast.fire({icon: 'error',title: result.message});
                        }
                    }
                });           
            }
          });
    }else{
        Swal.fire({
            title: 'Are you sure?',
            text: "This will restore the client",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, restore it!'
          }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'POST',
                    url: 'restore',
                    data: {id: id, table:table ,"_token": $('meta[name="_token"]').attr('content') },                                  
                    dataType: 'json',
                    success: function(result) {                        
                        if(!result.error){                        
                            Toast.fire({icon: 'success',title: result.message});  
                            $('.dataTable').DataTable().ajax.reload();                
                        }else{
                            Toast.fire({icon: 'error',title: result.message});
                        }
                    }
                });           
            }
          })
    }    
});