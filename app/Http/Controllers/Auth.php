<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
class Auth extends Controller
{

    function login(Request $req){    
        $user = User::where('email',$req->email)->get();                                
        if(isset($user) && !empty($user) ){            
            if(Crypt::decrypt($user[0]->password) == $req->password){
                session()->put('userdata',$user);
                $response['status'] = '"success"'; 
                $response['message'] = '"Logged In Successfully"';                                      
            }else{
                $response['status'] = '"error"'; 
                $response['message'] = '"Incorrect Login"';                                                     
            }
        }else{
            $response['status'] = '"error"'; 
            $response['message'] = '"Incorrect Login"';                          
        }   
        return redirect('/clients')->with(['status'=>$response['status'],'message'=>$response['message']]);        
    }

    function logout(Request $req){
        $req->session()->forget('userdata');
        return redirect('/login');
    }
    
    function delete(Request $request){
        try {
          $id = $request->id;
          $table = trim($request->table);          
          DB::table($table)->where(['id'=>$id])->update(['is_active'=>'0']);
          $response['error'] = false; 
          $response['message'] = 'Deleted Record';              
        } catch (\Illuminate\Database\QueryException $e) {            
            $response['error'] = true; 
            $response['message'] = 'Something went wrong';              
        }
        return response($response)->header('Content-Type', 'json');
    }

    function restore(Request $request){
        try {
          $id = $request->id;
          $table = trim($request->table);          
          DB::table($table)->where(['id'=>$id])->update(['is_active'=>'1']);
          $response['error'] = false; 
          $response['message'] = 'Record restored successfully';              
        } catch (\Illuminate\Database\QueryException $e) {            
            $response['error'] = true; 
            $response['message'] = 'Something went wrong';              
        }
        return response($response)->header('Content-Type', 'json');
    }

}
