<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company_detail;

class Company extends Controller
{
    //
    public function index(){        
        $companyData = Company_detail::select('field','value')->get();
        $data = [];
        for ($i=0; $i <count($companyData); $i++) {             
            $data[$companyData[$i]['field']] = $companyData[$i]['value'];
        }
        return view('pages.company',['company_details'=>$data]);
    }

    public function update(Request $request){        
        
        // $validator = $request->validate([
        //     "first_name" => 'required',        
        //     "last_name" => 'required',        
        //     "full_title" => 'required',        
        //     "website_address" => 'required',        
        //     "mail_address" => 'required',        
        //     "company_address" => 'required',        
        //     "cin_number" => 'required',        
        //     "pan_number" => 'required',        
        //     "gst_number" => 'required',        
        //     "iec_number" => 'required',        
        //     "mobile_number" => 'required',        
        //     "bank_name" => 'required',        
        //     "inr_account_number" => 'required',        
        //     "euro_account_number" => 'required',        
        //     "routing_number" => 'required',        
        //     "swift_code" => 'required',        
        //     "note_1" => 'required',        
        //     "note_2" => 'required',        
        //     "note_3" => 'required',        
        //     "thank_you_message" => 'required',        
        // ]);
        Company_detail::truncate();
        Company_detail::insert([
            array( "value" => $request["first_name"] ,"field" => "first_name"),        
            array( "value" => $request["last_name"] ,"field" => "last_name"),        
            array( "value" => $request["full_title"] , "field" => "full_title"),        
            array( "value" => $request["website_address"] , "field" => "website_address"),        
            array( "value" => $request["mail_address"] , "field" => "mail_address"),        
            array( "value" => $request["company_address"] , "field" => "company_address"),        
            array( "value" => $request["cin_number"] , "field" => "cin_number"),        
            array( "value" => $request["pan_number"] , "field" => "pan_number"),        
            array( "value" => $request["gst_number"] , "field" => "gst_number"),        
            array( "value" => $request["iec_number"] , "field" => "iec_number"),        
            array( "value" => $request["mobile_number"] , "field" => "mobile_number"),        
            array( "value" => $request["bank_name"] , "field" => "bank_name"),        
            array( "value" => $request["inr_account_number"] , "field" => "inr_account_number"),        
            array( "value" => $request["euro_account_number"] , "field" => "euro_account_number"),        
            array( "value" => $request["routing_number"] , "field" => "routing_number"),        
            array( "value" => $request["swift_code"] , "field" => "swift_code"),        
            array( "value" => $request["note_1"] , "field" => "note_1"),        
            array( "value" => $request["note_2"] , "field" => "note_2"),        
            array( "value" => $request["note_3"] , "field" => "note_3"),        
            array( "value" => $request["thank_you_message"] , "field" => "thank_you_message") 
        ]);

        $response['error'] = false; 
        $response['message'] = 'Data Updated succesfully';              
        $response['url'] = '/company';                                
        $request->session()->flash('status', "'success'");
        $request->session()->flash('message',"'".$response['message']."'");  
        return response($response)->header('Content-Type', 'json');  
    }
}

