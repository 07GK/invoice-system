<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Client as ClientModel;
use App\Models\Project;
use App\Models\Tax;

class Client extends Controller
{
    //
    function insert(Request $req){

        $validator = $req->validate([
            "name" => 'required',        
            "email" => 'required|email',        
            "phone_no" => 'required',        
            "address" => 'required',        
            "gstin" => 'required',        
            "place_of_supply" => 'required',        
            "currency" => 'required',        
            "state_code" => 'required',        
            "tax_name.*"  => "required",
            "tax_percentage.*"  => "required",
        ]);
        if (ClientModel::where('email', '=', $req->email)->exists()) {
            return back()->withInput()->with(['status'=>"'error'",'message'=>"'Client already exists !'"]);
        }
        
        $Client = ClientModel::create([
            'name' => $req->name,
            'email' => $req->email,
            'phone_no' => $req->phone_no,
            'address' => $req->address,
            'gstin' => $req->gstin,            
            'place_of_supply' => $req->place_of_supply,
            'currency' => $req->currency,
            'state_code' => $req->state_code,
            'is_active' => '1',
        ]);

        for ($i = 0; $i < count($req->tax_name); $i++) {
            $taxes[] = [
                'name' => $req->tax_name[$i],
                'percentage' => $req->tax_percentage[$i],                
                'client_id' => $Client->id,                
                'is_active' => '1',
            ];
        }
        Tax::insert($taxes);                                
        return redirect('/clients')->with(['status'=>"'success'",'message'=>"'Client added successfully !'"]);
        

    }

    function view(Request $req){
    
        // ## Read value
        $draw = $req->draw;
        $row = $req->start;
        $rowperpage = $req->length; // Rows display per page
        $columnIndex = $req->order[0]['column']; // Column index
        $columnName = $req->columns[$columnIndex]['data']; // Column name
        $columnSortOrder = $req->order[0]['dir']; // asc or desc
        $searchValue = $req->search['value']; // Search value

       
        // ## Total number of records without filtering
        
        $records = ClientModel::select('*');
        if(isset($_GET['flag']) && $_GET['flag'] =='deleted'){
            $records->where('is_active','0');
        }else{
            $records->where('is_active','1');
        }        
        $totalRecords = $records->get()->count();        
                
        // ## Fetch records                        
        $tableRecords = ClientModel::with('taxes')
        ->select('id','name','email','phone_no','address','gstin','place_of_supply','currency','state_code');
        if(isset($_GET['flag']) && $_GET['flag'] =='deleted'){
            $tableRecords->where('is_active','0');
        }else{
            $tableRecords->where('is_active','1');
        }
        $tableRecords->where(function($query) use($searchValue) {
            $query->orWhere('name','like','%'.$searchValue.'%')
                  ->orWhere('email','like','%'.$searchValue.'%')
                  ->orWhere('phone_no','like','%'.$searchValue.'%')
                  ->orWhere('address','like','%'.$searchValue.'%')
                  ->orWhere('gstin','like','%'.$searchValue.'%')
                  ->orWhere('place_of_supply','like','%'.$searchValue.'%')
                  ->orWhere('currency','like','%'.$searchValue.'%')
                  ->orWhere('state_code','like','%'.$searchValue.'%');
       });
       $totalRecordsWithFilter = $tableRecords->count();
       $tableRecords = $tableRecords->orderByRaw($columnIndex + 1 .' '.$columnSortOrder)->skip($row)->take($rowperpage)->get();
        


        $data = [];
        $i=$row+1;
        foreach ($tableRecords as $row) {
            if(isset($_GET['flag']) && $_GET['flag'] =='deleted'){
                $operation = "<a href='javascript:;' class='btn-success btn-sm m-1 restore' data-id='".$row['id']."' data-table='clients' title='Restore the client' ><i class='icon-1x text-white flaticon2-refresh-button ' ></i></a>";
            }else{
                $operation = "<a href='".url('clients/edit').'/'.$row['id']."' class='btn-warning btn-sm m-1' title='Edit the client' ><i class='icon-1x text-white flaticon-edit ' ></i></a>";
                $operation .= "<a href='javascript:;' class='btn-danger btn-sm delete' data-id='".$row['id']."' title='Delete the client' data-table='clients' ><i class='icon-1x text-white flaticon-delete ' ></i></a>";
            }
            $data[] = array($i,ucfirst($row['name']),ucfirst($row['email']),$row['phone_no'],$row['address'],$row['gstin'],$row['place_of_supply'],$row['currency'],$row['state_code'],$operation);             
        $i++;
        }
        
        // ## Response
        $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordsWithFilter,
        "aaData" => $data
        );

        return response($response)->header('Content-Type', 'json');        
    }

    function edit($id){         
        $clientData = ClientModel::where('clients.id',$id)->get();
        $taxData = Tax::where('client_id',$clientData[0]->id)->get();        
        return view('pages.edit-clients', ['clientData'=>$clientData,'taxData'=>$taxData]);
    } 

    function update(Request $req){ 
        $validator = $req->validate([
            "name" => 'required',        
            "email" => 'required|email',        
            "phone_no" => 'required',        
            "address" => 'required',        
            "gstin" => 'required',        
            "place_of_supply" => 'required',        
            "currency" => 'required',        
            "state_code" => 'required',        
            "tax_name.*"  => "required",
            "tax_percentage.*"  => "required",
        ]);

        if (ClientModel::where([['email', '=', $req->email],['id','!=',$req->editId]])->exists()) {
            return back()->withInput()->with(['status'=>"'error'",'message'=>"'Client already exists !'"]);
        }

        $ClientData = ClientModel::where('id',$req->editId)->update([
            'name' => $req->name,
            'email' => $req->email,
            'phone_no' => $req->phone_no,
            'address' => $req->address,
            'gstin' => $req->gstin,
            'phone_no' => $req->phone_no,
            'place_of_supply' => $req->place_of_supply,
            'currency' => $req->currency,
            'state_code' => $req->state_code,
            'is_active' => '1',
        ]);
        
        if(count($req->tax_name) > 0){
            $req->tax_name = array_values($req->tax_name);
            $req->tax_percentage = array_values($req->tax_percentage);
            Tax::where('client_id',$req->editId)->delete();    
        }
        for ($i = 0; $i < count($req->tax_name); $i++) {
            $taxes[] = [
                'name' => $req->tax_name[$i],
                'percentage' => $req->tax_percentage[$i],                                         
                'client_id' => $req->editId,                                         
                'is_active' => '1',
            ];
        }
        Tax::insert($taxes);          
        return redirect('/clients')->with(['status'=>"'success'",'message'=>"'Client updated successfully !'"]);
    } 

    function get_projects_by_client_id(Request $request){        
        // $projectData = Project::where('client_id',$request->id)->get();
        $clientData = ClientModel::with('projects','taxes')->where('id',$request->id)->get();                 
        if(empty($clientData)){
            $response['error']=true;
            $response['message']='No projects founded';
            $response['data']=[];
        }else{
            $response['error']=false;
            $response['message']='Data retrived successfully';
            $response['data']=$clientData;
        }
        
        return response($response)->header('Content-Type', 'json');
    }
}
