<?php

namespace App\Http\Controllers;

use App\Models\Client;

use Illuminate\Http\Request;
use Elibyy\TCPDF\Facades\TCPDF;
use App\Models\Company_detail;
use App\Models\invoice;
use App\Models\invoice_project;
use App\Models\invoice_tax;

class Invoices extends Controller
{
    public function index()
    {
        $clientData = Client::where('is_active', 1)->get();
        return view('pages.add-invoice', ['clientData' => $clientData]);
    }

    public function view(Request $request)
    {
         ## Read value
         $draw = $request->draw;
         $row = $request->start;
         $rowperpage = $request->length; // Rows display per page
         $columnIndex = $request->order[0]['column']; // Column index
         $columnName = $request->columns[$columnIndex]['data']; // Column name
         $columnSortOrder = $request->order[0]['dir']; // asc or desc
         $searchValue = $request->search['value']; // Search value
 
        
         ## Total number of records without filtering
         
         $records = invoice::join('clients','invoices.client_id','=','clients.id')->select('*');
         if(isset($_GET['flag']) && $_GET['flag'] =='deleted'){
             $records->where('invoices.is_active','0');
         }else{
             $records->where('invoices.is_active','1');
         }        
         $totalRecords = $records->get()->count();        
                 
         // ## Fetch records                        
         $tableRecords = invoice::join('clients','invoices.client_id','=','clients.id');                 
         if(isset($_GET['flag']) && $_GET['flag'] =='deleted'){
             $tableRecords->where('invoices.is_active','0');
         }else{
             $tableRecords->where('invoices.is_active','1');
         }
        //  $tableRecords->where(function($query) use($searchValue) {
        //      $query->orWhere('name','like','%'.$searchValue.'%')
        //            ->orWhere('email','like','%'.$searchValue.'%')
        //            ->orWhere('phone_no','like','%'.$searchValue.'%')
        //            ->orWhere('address','like','%'.$searchValue.'%')
        //            ->orWhere('gstin','like','%'.$searchValue.'%')
        //            ->orWhere('place_of_supply','like','%'.$searchValue.'%')
        //            ->orWhere('currency','like','%'.$searchValue.'%')
        //            ->orWhere('state_code','like','%'.$searchValue.'%');
        // });
        $totalRecordsWithFilter = $tableRecords->count();
        $tableRecords = $tableRecords->orderByRaw($columnIndex + 1 .' '.$columnSortOrder)->skip($row)->take($rowperpage)->get();         
 
         $data = [];
         $i=$row+1;
         foreach ($tableRecords as $row) {
             if(isset($_GET['flag']) && $_GET['flag'] =='deleted'){
                 $operation = "<a href='javascript:;' class='btn-success btn-sm m-1 restore' data-id='".$row['id']."' data-table='clients' title='Restore the client' ><i class='icon-1x text-white flaticon2-refresh-button ' ></i></a>";
             }else{
                 $operation = "<a href='".url('clients/edit').'/'.$row['id']."' class='btn-warning btn-sm m-1' title='Edit the client' ><i class='icon-1x text-white flaticon-edit ' ></i></a>";
                 $operation .= "<a href='javascript:;' class='btn-danger btn-sm delete' data-id='".$row['id']."' title='Delete the client' data-table='clients' ><i class='icon-1x text-white flaticon-delete ' ></i></a>";
             }
             $data[] = array($i,ucfirst($row['sr_no']),ucfirst($row['name']),$row['email'],$row['invoice_date'],$row['due_date'],$operation);             
         $i++;
         }
         
         // ## Response
         $response = array(
         "draw" => intval($draw),
         "iTotalRecords" => $totalRecords,
         "iTotalDisplayRecords" => $totalRecordsWithFilter,
         "aaData" => $data
         );
 
         return response($response)->header('Content-Type', 'json');        
    }

    public function save(Request $request)
    {
        // dd($request);
        $invoice = [
            'client_id' => $request->client_id,
            'sr_no' => '23/2312/3123',
            'invoice_date' => '2021/10/10',
            'due_date' => '2021/10/10',
        ];
        $invoice = invoice::create($invoice);

        for ($i = 0; $i < count($request->project_ids); $i++) {
            $invoice_project = [
                'invoice_id' => $invoice->id,
                'project_id' => $request->project_ids[$i],
                'project_desc' => $request->project_description[$i],
                'is_hourly_based' => $request->is_hourly_based[$i],
                'rate_per_hour' => ($request->is_hourly_based[$i]) ? $request->rate_per_hour[$i] : NULL,
                'total_hours' => ($request->is_hourly_based[$i]) ? $request->total_hours[$i] : NULL,
                'total_amount' => $request->total[$i],
            ];
            invoice_project::insert($invoice_project);
        }

        for ($i = 0; $i < count($request->project_ids); $i++) {
            $invoice_tax = [
                'invoice_id' => $invoice->id,
                'tax_name' => $request->tax_name[$i],
                'tax_per' => $request->tax_percentage[$i],
                'tax_amount' => $request->tax_amount[$i],
            ];
            invoice_tax::insert($invoice_tax);
        }
    }

    public function download()
    {
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf::SetTitle('Invoice');
        // set default monospaced font
        $pdf::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
        $pdf::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        // set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // set image scale factor
        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set font
        $pdf::SetFont('helvetica', '', 10);
        $pdf::AddPage('p', 'A3');
        $data = Company_detail::select('field', 'value')->get();
        $clientData = Client::with('projects', 'taxes')->where('id', $_POST['client_id'])->get()->toArray();
        $companyData = [];
        for ($i = 0; $i < count($data); $i++) {
            $companyData[$data[$i]['field']] = $data[$i]['value'];
        }

        $html = '<table border="1" cellpadding="5px">
                <tbody>
                            <tr>   
                            
                            <td colspan="4" rowspan="2" style="font-size:30px; text-align: center; vertical-align: middle;">' . $companyData["full_title"] . '<br>' . '<span style="font-size:14px;">Web: <span style="color: #0000EE;">' . $companyData["website_address"] . '  </span> | Mail: ' . $companyData["website_address"] . '</span></td>
                            <td style="text-align: center; vertical-align: middle;"><h1>INVOICE</h1></td>   
                        </tr>
                        <tr >                 
                            <td style=" background-color:#E8E8E8; text-align: center; vertical-align: middle;"><h4>TAX Invoice</h4></td>   
                        </tr>
                        </tbody>
            </table>';

                    $html .= ' <div class="space"></div>
                        
                    <table border="1" cellpadding="5px">
                        <tbody>
                            
                            <tr><td width="100%">
                            <table border="0">
                                <tr>
                                <td width="200" rowspan="5" colspan="3" ><h3><u>COMPANY :</u></h3><br>' . $companyData["company_address"] . '</td>
                                </tr>
                            </table>
                        </td> </tr>                
                        </tbody>
            </table>
                    ';

                    $html .= '<div class="space"></div>
            <table cellpadding="3" style=" border-collapse: collapse; border: 1px solid black;">
                <tr>
                    <td style="font-size:14px;">CIN Number:</td>
                    <td>' . $companyData["cin_number"] . '</td>
                    <td style="font-size:14px;">Invoice No.:</td>
                    <td style="font-size:14px;">1</td>
                </tr>
                <tr>
                    <td style="font-size:14px;">Company PAN Number:</td>
                    <td>' . $companyData["pan_number"] . '</td>
                    <td style="font-size:14px;">Invoice Date:</td>
                    <td style="font-size:14px;">' . $_POST["invoice_date"] . '</td>
                </tr>
                <tr>
                    <td style="font-size:14px;">Company GST Number:</td>
                    <td>' . $companyData["gst_number"] . '</td>
                    <td style="font-size:14px;">Due Date:</td>
                    <td style="font-size:14px;">' . $_POST["due_date"] . '</td>
                </tr>
                <tr>
                    <td style="font-size:14px;">Company IEC Number:</td>
                    <td>' . $companyData["iec_number"] . '</td>
                    <td style="font-size:14px;"></td>
                    <td style="font-size:14px;"></td>
                </tr>
                </table>';

                    $html .= '<div class="space"></div>
                <table border="1" cellpadding="5">
                    <tr>
                        <td style="font-size:14px; background-color:#E8E8E8;">BILL TO</td>
                        <td style="font-size:14px; background-color:#E8E8E8;">ADDRESS</td>
                        <td style="font-size:14px; background-color:#E8E8E8;">PLACE OF SUPPLY</td>
                        <td style="font-size:14px; background-color:#E8E8E8;">STATE CODE</td>
                    </tr>
                    <tr>
                        <td rowspan="2" style="font-size:14px;">' . $clientData[0]['name'] . " <br>" . $clientData[0]['email'] . "<br>GST No: <br>" . $clientData[0]['gstin'] . '</td>
                        <td rowspan="2" style="font-size:14px;">' . $clientData[0]["address"] . '</td>
                        <td rowspan="2" style="font-size:14px;">' . $clientData[0]["place_of_supply"] . '</td>
                        <td rowspan="2" style="font-size:14px;">' . $clientData[0]["state_code"] . '</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>';

                    $html .= ' <div class="space"></div>
                        
                    <table border="1" cellpadding="5px">
                        <tbody>
                            <tr style="background-color:#E8E8E8;">
                                <th>No.</th>                    
                                <th colspan="2">Project Description</th>
                                <th>Hours</th>
                                <th>Rate</th>
                                <th>Amount</th>   
                            </tr>';

                    for ($i = 0; $i < count($_POST['project_ids']); $i++) {
                        $html .= '<tr><td>' . ($i + 1) . '</td>';
                        $html .= '<td colspan="2">' . $_POST['project_description'][$i] . '</td>';
                        $html .= '<td>' . $_POST['total'][$i] . '</td>';
                        $html .= '<td>' . $clientData[0]['currency'] . " " . $_POST['rate_per_hour'][$i] . '/-</td>';
                        $html .= '<td>' . $clientData[0]['currency'] . " " . $_POST['total'][$i] . '/-</td></tr>';
                    }

                    for ($j = 0; $j < count($_POST['tax_name']); $j++) {
                        // if($j != count($_POST['tax_name'])){
                        $html .= '<tr><td></td>';
                        // $html .= '<td></td>';
                        $html .= '<td colspan="2"></td>';
                        $html .= '<td>' . $_POST['tax_name'][$j] . '</td>';
                        $html .= '<td>' . $_POST['tax_percentage'][$j] . '%</td>';
                        $html .= '<td>' . $clientData[0]['currency'] . " " . $_POST['tax_amount'][$j] . '/-</td></tr>';
                        // }else{
                        //     $pdf .= '<tr style="background-color:#E8E8E8;">
                        //             <td colspan="6"><b>Total Invoice Amount:</b></td>';
                        //     $pdf .= '<td>'. $row1['currency'] ." ".$_POST['total_with_tax'].'/-</td></tr>';
                        // }
                    }

                    // $pdf .= '<tr><td colspan="7" style="background-color:#E8E8E8;"><b>Amount in words: </b>'.test($_POST['total_with_tax']).'</td></tr>';
                    $total = array_sum($_POST['total']);
                    $tax_amount = array_sum($_POST['tax_amount']);
                    $html .= '<tr><td colspan="5" style="background-color:#E8E8E8;"><b>Total Amount </b></td><td style="background-color:#E8E8E8;">' . $clientData[0]['currency'] . '  ' . ($total - $tax_amount) . '</td></tr>';
                    $html .= '<tr><td colspan="7" style="background-color:#E8E8E8;"><b>Total Amount In Words: </b> - ' . $this->digitsToWords(($total - $tax_amount)) . ' </td></tr>';
                    $html .= '<tr><td colspan="7" style="background-color:#E8E8E8;"><b>Payment Milestone: </b> - Montly payment upon invoice</td></tr>';

                    $html .= '</tbody></table>';

                    $html .= ' <div class="space"></div>
                        
                    <table border="1" cellpadding="5px">
                        <tbody>
                            <tr>
                                <td colspan="6" style="background-color:#E8E8E8;" ><b>Bank Details: ' . $companyData["full_title"] . '</b></td>   
                            </tr>
                            <tr>
                                <td rowspan="8" colspan="2" width="200" style="font-size:14px;line-height:20px;text-align:left;">
                                <ul style="list-style-type:none;text-align:left;">                    
                                        <li style="font-size:14px;">Bank Name</li>
                                        <li style="font-size:14px;">INR Account No.</li>
                                        <li style="font-size:14px;">EURO Account NO. </li>
                                        <li style="font-size:14px;">Routing Number </li>
                                        <li style="font-size:14px;">Swift Code </li>
                                        <li style="font-size:14px;">Mobile Number </li>                          
                                </ul>
                                </td>
                                <td rowspan="8"  colspan="2" width="200" style="font-size:14px;line-height:20px;text-align:left;">
                                <ul style="list-style-type:none;">
                                
                                <li style="font-size:14px;">' . $companyData["bank_name"] . '</li>
                                <li style="font-size:14px;">' . $companyData["inr_account_number"] . '</li>
                                <li style="font-size:14px;">' . $companyData["euro_account_number"] . '</li>
                                <li style="font-size:14px;">' . $companyData["routing_number"] . '</li>
                                <li style="font-size:14px;">' . $companyData["swift_code"] . '</li>
                                <li style="font-size:14px;">' . $companyData["mobile_number"] . '</li>                          
                        </ul>
                        </td>
                                <td rowspan="8" colspan="4" width="385" style="font-size:14px; text-align: left; vertical-align: middle;line-height:20px;">                    
                                <b>NOTE:</b>                    
                                    <ul>
                                        <li style="font-size:14px;">' . $companyData["note_1"] . '</li>
                                        <li style="font-size:14px;">' . $companyData["note_2"] . '</li>
                                        <li style="font-size:14px;">' . $companyData["note_3"] . '</li>
                                    </ul>
                                    
                                </td>           
                            </tr>
                        
                        </tbody>
            </table>';

                    $html .= '<div class="space"></div><table border="1" cellpadding="5px">
                        <tbody>
                            <tr>   
                            <td rowspan="4" style="text-align: center; vertical-align: middle;"><br>' . $companyData["thank_you_message"] . '<br><br>' . $companyData["first_name"] . " " . $companyData["last_name"] . ' | Director</td>
                            <td rowspan="4"><br><br><br></td>
                        </tr>
                        </tbody>
            </table>';

        $pdf::writeHTML($html, true, false, true, false, '');
        $pdf::Output('hello_world.pdf');
    }


    function digitsToWords(float $number)
    {
        $no = floor($number);
        $point = round($number - $no, 2) * 100;
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array(
            '0' => '', '1' => 'One', '2' => 'Two',
            '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
            '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
            '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
            '13' => 'Thirteen', '14' => 'Fourteen',
            '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
            '18' => 'Eighteen', '19' => 'Nineteen', '20' => 'Twenty',
            '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
            '60' => 'Sixty', '70' => 'Seventy',
            '80' => 'Eighty', '90' => 'Ninety'
        );
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str[] = ($number < 21) ? $words[$number] .
                    " " . $digits[$counter] . $plural . " " . $hundred
                    :
                    $words[floor($number / 10) * 10]
                    . " " . $words[$number % 10] . " "
                    . $digits[$counter] . $plural . " " . $hundred;
            } else $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);
        $points = ($point) ?
            "." . $words[$point / 10] . " " .
            $words[$point = $point % 10] : '';
        // return $result . "Rupees  " . $points . " Paise";
        return $result . "Rupees  ";
    }
}
