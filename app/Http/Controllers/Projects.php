<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Client;

class Projects extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('pages.view-projects',['clientData'=>Client::select('id','name')->where('is_active',1)->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        return view('pages.add-projects',['clientData'=>Client::select('id','name')->where('is_active',1)->get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = [
            'name' => 'required',
            'desc' => 'required',            
            'client_id' => 'required|numeric',            
        ];
        if($request->has('is_hourly_based')){
            $validate['rate_per_hour'] = 'required';
        }        
        $request->validate($validate);

        $project = Project::create([
            'client_id'=>$request->client_id,
            'name'=>$request->name,
            'desc'=>$request->desc,
            'is_hourly_based'=>($request->has('is_hourly_based')) ? 1 : 0,
            'rate_per_hour'=>$request->rate_per_hour,
        ]);
                                        
        return redirect('/projects')->with(['status'=>"'success'",'message'=>"'Project added successfully !'"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clientData  = Client::select('id','name')->where('is_active','1')->get();
        $projectData = Project::where('id',$id)->get();
        return view('pages.edit-projects', ['clientData'=>$clientData,'projectData'=>$projectData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = [
            'name' => 'required',
            'desc' => 'required',            
        ];
        if($request->has('is_hourly_based')){
            $validate['rate_per_hour'] = 'required';
        }        
        $request->validate($validate);

        $project = Project::where('id',$id)->update([
            'client_id'=>$request->client_id,
            'name'=>$request->name,
            'desc'=>$request->desc,
            'is_hourly_based'=>($request->has('is_hourly_based')) ? 1 : 0,
            'rate_per_hour'=>($request->has('is_hourly_based')) ?$request->rate_per_hour : NULL,
        ]);

        return redirect('/projects')->with(['status'=>"'success'",'message'=>"'Project updated successfully !'"]); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function view(Request $request){
            
        $draw = $request->draw;
        $row = $request->start;
        $rowperpage = $request->length; // Rows display per page
        $columnIndex = $request->order[0]['column']; // Column index
        $columnName = $request->columns[$columnIndex]['data']; // Column name
        $columnSortOrder = $request->order[0]['dir']; // asc or desc
        $searchValue = $request->search['value']; // Search value
      
       
        // ## Total number of records without filtering
        
        $records = Project::select('*');        
        if(isset($_GET['flag']) && $_GET['flag'] =='deleted'){
            $records->where('is_active','0');
        }else{
            $records->where('is_active','1');
        }                
        $totalRecords = $records->get()->count();
                
        // ## Fetch records                        
        $tableRecords = Project::select('*');
        if(isset($_GET['flag']) && $_GET['flag'] =='deleted'){
            $tableRecords->where('is_active','0');
        }else{
            $tableRecords->where('is_active','1');
        }        
        $tableRecords->where(function($query) use($searchValue) {
            $query->orWhere('name','like','%'.$searchValue.'%')
            ->orWhere('desc','like','%'.$searchValue.'%')              
            ->orWhere('rate_per_hour','like','%'.$searchValue.'%');
        });        

        if($_GET['client_id']){
            $tableRecords->where('client_id',$_GET['client_id']);     
        }

        $totalRecordwithFilter = $tableRecords->count();
        $tableRecords = $tableRecords->orderByRaw($columnIndex + 1 .' '.$columnSortOrder)->skip($row)->take($rowperpage)->get();
        
        $data = [];
        $i=$row+1;
        foreach ($tableRecords as $row) {
            if(isset($_GET['flag']) && $_GET['flag'] =='deleted'){
                $operation = "<a href='javascript:;' class='btn-success btn-sm m-1 restore' data-id='".$row['id']."' data-table='projects' title='Restore the client' ><i class='icon-1x text-white flaticon2-refresh-button ' ></i></a>";
            }else{
                $operation = "<a href='".url('projects/'.$row['id'].'/edit')."' class='btn-warning btn-sm m-1' ><i class='icon-1x text-white flaticon-edit ' ></i></a>";            
                $operation .= "<a href='javascript:;' class='btn-danger btn-sm delete' data-id='".$row['id']."' data-table='projects' ><i class='icon-1x text-white flaticon-delete ' ></i></a>";
            }
            $data[] = array($i,ucfirst($row['name']),ucfirst($row['desc']),($row['is_hourly_based']==1)?'Yes':'No',($row['rate_per_hour'])?$row['rate_per_hour']:'N/A',$operation);             
        $i++;
        }
        
        // ## Response
        $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordwithFilter,
        "aaData" => $data
        );

        return response($response)->header('Content-Type', 'json');
    }
}
