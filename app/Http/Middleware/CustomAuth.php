<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CustomAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->session()->has('userdata')){
            if($request->path() == 'login' ){
                return redirect('/clients');
            }else{
                return $next($request);
            }
        }else{
            if($request->path() != 'login' ){
                return redirect('/login');
            }else{
                return $next($request);
            }
        }
    }
}
