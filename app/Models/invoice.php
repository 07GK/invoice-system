<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class invoice extends Model
{
    use HasFactory;
    protected $fillable = [
        'client_id',
        'sr_no',        
        'invoice_date',        
        'due_date',        
    ];

    
    public function projects()
    {
      return $this->hasMany(invoice_project::class);
    }
    public function taxes()
    {
      return $this->hasMany(invoice_tax::class);
    }
}
