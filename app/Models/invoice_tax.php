<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class invoice_tax extends Model
{
    use HasFactory;
    protected $fillable = [
        'invoice_id',        
        'tax_name',        
        'tax_per',                
        'tax_amount',        
    ];
}
