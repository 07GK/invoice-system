<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class invoice_project extends Model
{
    use HasFactory;
    protected $fillable = [
        'invoice_id',        
        'project_id',        
        'project_desc',        
        'is_hourly_based',        
        'rate_per_hour',        
        'total_hours',        
        'total_amount',        
    ];
}
