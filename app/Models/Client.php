<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;
    protected $fillable = [
        'name' ,
        'email',
        'phone_no',
        'address' ,
        'gstin' ,
        'phone_no',
        'place_of_supply',
        'currency' ,
        'state_code',
        'is_active' ,
    ];

    public function projects()
    {
      return $this->hasMany(Project::class);
    }
    public function taxes()
    {
      return $this->hasMany(Tax::class);
    }
    public function invoices()
    {
      return $this->hasMany(invoice::class);
    }

}
